package praktikum2;

import lib.TextIO;

public class JagatisJaJ22k {
public static void main(String[] args) {
	int inimesed;
	System.out.println("Palun sisesta inimeste arv");
	inimesed=TextIO.getlnInt();
	
	int grupp;
	System.out.println("Palun sisesta ühe grupi suurus");
	grupp=TextIO.getlnInt();
	
	// jäägiga jagamine on %
	// 7 % 3 => 1
	
	int gruppideArv = (int) Math.floor(inimesed / grupp);
	int jääk = inimesed % grupp;
	System.out.println("Saab moodustada " + gruppideArv + " gruppi" + ", jääk on " + jääk);
}
}
