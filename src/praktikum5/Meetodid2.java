package praktikum5;

import lib.TextIO;

public class Meetodid2 {
	public static void main(String[] args) {

		int kasutajaSisestas = kasutajaSisestus(1, 10);
		System.out.println("kasutajaSisestus meetod tagastas: " + kasutajaSisestas);

	}

	public static int kasutajaSisestus(int min, int max) {
		// TODO: kui number ei ole vahemikus, siis küsida uuesti
		while (true) {
			System.out.println("Palun sisesta number vahemikus " + min + " kuni " + max);
			int sisestus = TextIO.getlnInt();
			if (sisestus >= min && sisestus <= max) {

				return sisestus;
			} else {
				System.out.println("Vigane sisestus! Proovi uuesti.");
				return 0;
			}
		}

		// või nii: public static int kasutajaSisestus(int min, int max) {
		//
		// int sisestus;
		// do {
		// System.out.println("Palun sisesta number vahemikus " + min + " kuni "
		// + max);
		// sisestus = TextIO.getlnInt();
		// } while (sisestus < min || sisestus > max);
		// return sisestus;
		// }
	}
}
