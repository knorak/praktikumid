package praktikum5;

import lib.TextIO;

public class Meetodid3 {
	public static void main(String[] args) {

		int kasutajaArvamus = Meetodid3.kasutajaSisestus("Sisesta kull (0) või kiri (1)", 0, 1);
		int arvutiMyndivise = suvalineArv(0, 1);
		System.out.println("arvuti arvas" + arvutiMyndivise);
		if (kasutajaArvamus == arvutiMyndivise) {
			System.out.println("Sina võitsid!");
		}
	}

	public static int kasutajaSisestus(String string, int min, int max) {
		while (true) {
			System.out.println("Palun sisesta number vahemikus " + min + " kuni " + max);
			int sisestus = TextIO.getlnInt();
			if (sisestus >= min && sisestus <= max) {

				return sisestus;
			} else {
				System.out.println("Vigane sisestus! Proovi uuesti.");
				return 0;
			}
		}
	}

	public static int suvalineArv(int min, int max) {

		int vahemik = max - min;

		return min + (int) (Math.random() * (vahemik + 1));
	}
}
