package kontrollt662;

public class KT2 {

	public static void main(String[] args) {
		System.out.println(score(new int[] { 4, 3, 2, 1, 0 }));
	}

	public static int score(int[] points) {

		int summa = 0;
		int min1 = points[0];
		int min2 = points[1];

		for (int j = 0; j < points.length; j++) {
			if (min1 > points[j]) {
				min1 = points[j];
			}
		}

		for (int j = 0; j < points.length; j++) {
			if (min2 > points[j]) {
				min2 = points[j];
			}
		}
		for (int j = 0; j < points.length; j++) {
			summa = points[j] + summa;
		}

		int vastus = summa - min1 - min2;

		return vastus;
	}
}
