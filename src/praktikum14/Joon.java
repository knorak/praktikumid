package praktikum14;

public class Joon {

	Punkt algPunkt;
	Punkt l6ppPunkt;

	public Joon(Punkt i, Punkt j) {
		algPunkt = i;
		l6ppPunkt = j;
	}

	@Override
	public String toString() {

		return "Joon punktist " + algPunkt + " punkti " + l6ppPunkt;
	}

	public double Pikkus() {
		// TODO: kasutame Pythagorase teoreemi

		double joonePikkus = Math.sqrt((algPunkt.x - l6ppPunkt.x) * (algPunkt.x - l6ppPunkt.x)
				+ (algPunkt.y - l6ppPunkt.y) * (algPunkt.y - l6ppPunkt.y));
		return joonePikkus;

	}

}
