package praktikum14;

public class Ring {

	Punkt keskpunkt;
	double raadius;

	public Ring(Punkt i, double r) {
		keskpunkt = i;
		raadius = r;
	}

	@Override
	public String toString() {

		return "Ring raadiusega (" + raadius + ") ning keskpunktiga " + keskpunkt;
	}

	public double Ymberm66t(){

		double YmberM66t = 2 * raadius * Math.PI;
		return YmberM66t;
	}
	
	public double Pindala() {
		
		return Math.PI * raadius * raadius;
	}
}
