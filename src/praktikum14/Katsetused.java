package praktikum14;

public class Katsetused {

	public static void main(String[] args) {

		Punkt minuPunkt = new Punkt();

		minuPunkt.x = 40;
		minuPunkt.y = 100;

		Punkt veelYksPunkt = new Punkt(100, 40);
		System.out.println(veelYksPunkt);

		
		
		Joon minuJoon = new Joon(minuPunkt, veelYksPunkt);

		System.out.println(minuJoon);	
		System.out.println("Joone pikkus on " + minuJoon.Pikkus());
		
		//Joon.Pikkus toimiks siis kui meetod oleks staatiline
		
		
		
		Ring minuRing = new Ring(minuPunkt, 10);
		
		System.out.println(minuRing);
		System.out.println("Ringi ümbermõõt on " + minuRing.Ymberm66t());
		System.out.println("Ringi pindala on " + minuRing.Pindala());
	}

}
