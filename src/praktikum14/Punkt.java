package praktikum14;

public class Punkt {

	int x;
	int y;

	public Punkt(int x, int y) {
		this.x = x;
		this.y = y;
	}

	// public Punkt (int i, int j) {
	// x = i;
	// y = j;
	// }

	 public Punkt() {
	// Luuakse Punkt objekt koordinaate määramata
	 }

	@Override
	public String toString() {
		// TODO: trükkida välja punkt kuidagi kenasti
		// mis eclipse ise kirjutab: " return super.toString(); "
		
		return "Punkt(" + x + ", " + y + ")";
		
	}

}
