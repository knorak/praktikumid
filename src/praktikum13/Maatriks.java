package praktikum13;

import java.util.ArrayList;
import java.util.concurrent.ForkJoinPool.ForkJoinWorkerThreadFactory;

public class Maatriks {

	public static ArrayList<ArrayList<Double>> transponeeri(ArrayList<ArrayList<Double>>){
		//TODO: maatriksi transponeerimine
		return sisendMaatriks;
	}
//	public static double[][] transposeMatrix(double[][] m) {
//		double[][] temp = new double[m[0].length][m.length];
//		for (int i = 0; i < m.length; i++)
//			for (int j = 0; j < m[0].length; j++)
//				temp[j][i] = m[i][j];
//		return temp;
//	}
	
	public static void main(String[] args) {

		// double[][]
		// ArrayList<ArrayList<Double>>
		// ArrayList<double[]>

		String kataloogitee = Maatriks.class.getResource(".").getPath();
		ArrayList<String> failiSisu = FailiLugeja.loeFail(kataloogitee + "Maatriks.txt");
		System.out.println(failiSisu);

		ArrayList<ArrayList<Double>> maatriks = new ArrayList<ArrayList<Double>>();

		for (String rida : failiSisu) {
			String[] elemendid = rida.split(" ");
			ArrayList<Double> maatriksiRida = new ArrayList<Double>();
			for (String el : elemendid) {
				double nr = Double.parseDouble(el);
				maatriksiRida.add(nr);

			}
			maatriks.add(maatriksiRida);

		}
		System.out.println(maatriks);

	}


}
