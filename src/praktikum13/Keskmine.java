package praktikum13;

import java.util.ArrayList;

public class Keskmine {

	public static void main(String[] args) {
		String kataloogitee = Keskmine.class.getResource(".").getPath();
		ArrayList<String> failiSisu = FailiLugeja.loeFail(kataloogitee + "numbrid.txt");
		System.out.println(failiSisu);

		// TODO: Aritmeetiline keskmine

		double summa = 0;
		int vigaseidRidu = 0;

		for (String rida : failiSisu) {
			// TODO: leia summa
			try {
				double nr = Double.parseDouble(rida);
				summa += nr;
			} catch (NumberFormatException e) {
				System.out.println("See ei ole number " + "(" + rida + ")");
				vigaseidRidu++;

			}
		}
		System.out.println("Summa on " + summa);

		// TODO: arvutada elementide keskmine
		
	double keskmine = summa / (failiSisu.size() - vigaseidRidu);
	
	System.out.println("Keskmine on " + keskmine);

	}

}
