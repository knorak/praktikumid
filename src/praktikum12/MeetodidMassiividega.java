package praktikum12;

public class MeetodidMassiividega {

	public static void main(String[] args) {

		int[] mingiMassiiv = { 4, 5, 7, 9 };
		// tryki(mingiMassiiv);

		int[][] kahem22tmeline = { { 1, 1, 1, 1, 1 }, { 2, 3, 4, 5, 6 }, { 3, 4, 5, 6, 7 }, { 4, 5, 6, 7, 8 },
				{ 5, 6, 7, 8, 9 }, };

		// tryki(kahem22tmeline);

		tryki(ridadeSummad(kahem22tmeline));

	}

	public static int reaSumma(int[] rida) {

		int summa = 0;
		for (int i : rida) {
			summa += i;

		}
		return summa;
	}

	public static int[] ridadeSummad(int[][] maatriks) {

		int[] summad = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			summad[i] = reaSumma(maatriks[i]);
		}
		return summad;
	}

	public static void tryki(int[] massiiv) {

		for (int i = 0; i < massiiv.length; i++) {
			System.out.print(massiiv[i] + " ");
		}
		System.out.println();
	}

	public static void tryki(int[][] maatriks) {
		for (int i = 0; i < maatriks.length; i++) {
			tryki(maatriks[i]);
		}

	}

}