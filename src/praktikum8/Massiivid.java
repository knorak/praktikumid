package praktikum8;

public class Massiivid {

	public static void main(String[] args) {

		int arv; // muutuja deklaratsioon
		// int arv = 7;
		int[] arvud = new int[10]; // new int näitab mitu arvu on massiivis
		// int[] arvud = {3, 5, 7, 9, 11};
		
		
		//Väärtustamine
		arv = 7;
		
		arvud[0] = 7; //[0] tähendab kohal 0 (nullist üheksani, kunagi pole kümme)
		arvud[1] = 12;
		
		//Väljustamine
		//System.out.println(arv);
		//System.out.println(arvud[1]);
		
		for (int i = 0; i < arvud.length; i++) {
			System.out.println(arvud[i]);
		}
	}

}
