package praktikum8;

import lib.TextIO;

public class S6napikkus {

	public static void main(String[] args) {

		String[] s6nad = new String[10];
		System.out.println("Palun sisesta 10 sõna!");

		for (int i = 0; i < s6nad.length; i++) {
			s6nad[i] = TextIO.getlnString();
		}

		for (int i = 0; i < s6nad.length; i++) {
			System.out.println(s6nad[i].length() + " " + s6nad[i]);
		}

	}
}
