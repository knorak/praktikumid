package praktikum8;

import java.util.ArrayList;

import lib.TextIO;

public class InimeseN2ited {

	public static void main(String[] args) {
		
		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();
		
		while(true){
			
			System.out.println("Palun sisesta nimi ja vanus");
			String nimi = TextIO.getlnString();
			if (nimi.equals(""))
				break;
			int vanus = TextIO.getlnInt();
			
			Inimene keegi = new Inimene(nimi, vanus);
			inimesed.add(keegi);
			
		}
		
		for(Inimene inimene : inimesed) {
			// java kutsub välja Inimene klassi toString() meetodi
			inimene.tervita();
			
		}

//		String nimi = new String("Mati");
//		
//		Inimene keegi = new Inimene("Kati", 34);
//		keegi.tervita();
//		Inimene keegiVeel = new Inimene("Mati", 23);
//		keegiVeel.tervita();
//		
//		System.out.println(keegi);
	}

}
