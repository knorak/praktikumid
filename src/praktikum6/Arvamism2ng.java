package praktikum6;

import lib.TextIO;

public class Arvamism2ng {

	public static void main(String[] args) {
		
		int arvutiArv = arvutiArv(1, 100);
		
		
		while (true){
			System.out.println("Arva ära, mis arv on? ");
			
			int kasutajaArv = TextIO.getlnInt();
			
			if (arvutiArv == kasutajaArv) {
				System.out.println("Arvasid ära! Jehuuu!");
				break;
			} 
			else if (kasutajaArv > arvutiArv) {
				System.out.println("Vale, arvuti arv on väiksem");
			}
			else {
				System.out.println("Vale, arvuti arv on suurem");
			}
			
		}
	}

	

	public static int arvutiArv(int min, int max) {
		
		int vahemik = max - min + 1;
		return (int) (Math.random() * vahemik) + min;
	
}
}

