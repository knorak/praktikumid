package praktikum6;

import lib.TextIO;
import praktikum5.Meetodid2;
import praktikum5.Meetodid3;

public class KullJaKiri {

	public static void main(String[] args) {

		int kasutajaRaha = 100;
		System.out.println("Kui suure panuse teed(max 25 raha)?");

		while (true) {
			int panus = Meetodid2.kasutajaSisestus(1, 25);

			int myndivise = Meetodid3.suvalineArv(0, 1);
			
			if (myndivise == 0) {
				System.out.println("Võitsid, saad raha topelt tagasi! Jehuu!");
				kasutajaRaha += panus * 2;
			} else {
				System.out.println("Kaotasid..");
			}
			System.out.println("Sul on " + kasutajaRaha + " raha.");

		}

	}
}
