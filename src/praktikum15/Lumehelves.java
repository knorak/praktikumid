package praktikum15;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Lumehelves {
	
	int x;
	int y;
	
	public Lumehelves(int x, int y) {
		this.x = x;
		this.y = y;

	}

	
	public void joonista(GraphicsContext gc) {
		gc.setStroke(Color.BLUE);
		gc.strokeLine(x-10, y-10, x+10, y+10);
		gc.strokeLine(x+10, y-10, x-10, y+10);
		gc.strokeLine(x, y-7, x, y+7);
		gc.strokeLine(x-7, y, x+7, y);
		
		//vasak ülemine
		gc.strokeLine(x-4, y-10, x-4, y-4);
		gc.strokeLine(x-10, y-4, x-4, y-4);
		
		// parem alumine
		gc.strokeLine(x+4, y+10, x+4, y+4);
		gc.strokeLine(x+10, y+4, x+4, y+4);
		
		// parem ülemine
		gc.strokeLine(x+4, y-4, x+4, y-10);
		gc.strokeLine(x+4, y-4, x+10, y-4);
		
		// vasak alumine
		gc.strokeLine(x-4, y+4, x-10, y+4);
		gc.strokeLine(x-4, y+4, x-4, y+10);
	}

}
