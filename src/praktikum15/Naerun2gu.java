package praktikum15;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

public class Naerun2gu {
	
	int x;
	int y;

	public Naerun2gu(int x, int y) {
		this.x = x;
		this.y = y;

	}

	public void joonistaN2gu(GraphicsContext gc) {
		
		gc.setFill(Color.PINK);
		gc.fillRoundRect(x, y, 50, 50, 50, 50);
		gc.setFill(Color.RED);
		gc.fillRoundRect(x+10, y+10, 10, 10, 10, 10);
		gc.fillRoundRect(x+30, y+10, 10, 10, 10, 10);
		gc.fillArc(x+9, y+9, 30, 30, 220, 100, ArcType.OPEN);

		


		
	}
}
