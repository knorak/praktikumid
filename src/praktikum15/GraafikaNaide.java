package praktikum15;
import com.sun.javafx.geom.Rectangle;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class GraafikaNaide extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("JavaFX-iga joonistamise nÃ¤ide");
		Group root = new Group();
		Canvas canvas = new Canvas(400, 400);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		joonista(gc);
		root.getChildren().add(canvas);
		primaryStage.setScene(new Scene(root));
		primaryStage.show();
		
	}

	private void joonista(GraphicsContext gc) {

		
//		Naerun2gu n2gu = new Naerun2gu(200, 100);
//		n2gu.joonistaN2gu(gc);
		
//		Lumehelves lumi = new Lumehelves(100, 100);
//		lumi.joonista(gc);
		
		new GraafilineLumesadu(20, gc);

	}
}